
  %% Enhancing image for detection of outside

Cell_whole = I .* uint8(maskOutside);
Cell_whole_adj=zeros(size(Cell_whole));
for k=kvec
    Cell_whole_adj(:,:,k)=imsharpen(Cell_whole(:,:,k),'Radius',50,'Amount',2);
    %figure;imshowpair(Cell_whole,Cell_whole_adj,'montage');
end


%% Finding cell membrane with Active Contour
maskOutsideAc=false(size(I));
maskInsideAc=false(size(I));
maskBorderAc=false(size(I));
                
maskOutsideAc(:,:,k)=activecontour(Cell_whole_adj(:,:,k),maskOutside(:,:,k),iterations,'edge','SmoothFactor',SFOutside,'ContractionBias',CBOutside);
maskOutsideAc(:,:,k)=imerode(maskOutsideAc(:,:,k),strel('disk',3*ceil(res)));   % 
maskOutsideAc(:,:,k)=imdilate(maskOutsideAc(:,:,k),strel('disk',3*ceil(res)));   %
maskOutsideAc(:,:,k)=bwareaopen(maskOutsideAc(:,:,k),ceil(1000*res)); %Removes elemets smaller than 1000*res no. of pixles

maskFill=(maskOutsideAc(:,:,k) &~ imerode(maskOutsideAc(:,:,k),strel('disk',ceil(0.5*res))));   % closing holes in the membrane
I_filled=Cell_whole(:,:,k)+Cell_whole(:,:,k).* uint8(maskFill)*uint8max;
I_filled(I_filled<0)=0;
I_filled(I_filled>uint8max)=uint8max;

Cell_tmp=I_filled .* uint8(maskOutsideAc(:,:,k));
Cell_tmp=imcomplement(Cell_tmp);
Cell_tmp=Cell_tmp>round(mean(Cell_tmp(maskOutsideAc(:,:,k))));
Cell_tmp(~maskOutsideAc(:,:,k))=0;
Cell_tmp=bwareaopen(Cell_tmp,round(40*res));
Cell_tmp=Cell_tmp .* maskOutsideAc(:,:,k);
Cell_tmp=imdilate(Cell_tmp,strel('disk',round(2*res)));
Cell_tmp(1:size(crop1,2))=0;
Cell_tmp(1:size(crop1,2),size(crop2))=0;
Cell_tmp(1,1:size(crop2,2))=0;
Cell_tmp(size(crop1),1:size(crop2,2))=0;
Cell_tmp=imerode(Cell_tmp,strel('disk',round(3*res)));
maskInsideAc(:,:,k)=Cell_tmp;
maskBorderAc(:,:,k) = (maskOutsideAc(:,:,k)&~maskInsideAc(:,:,k));
maskBorderAc(:,:,k) = bwareaopen(maskBorderAc(:,:,k),ceil(1000*res)); %Removes elements smaller than x*res from the border


%% Binarize  
Cell_Bin=false(size(I));
Cell_interior_Bin=false(size(I));
NeighborhoodSize = 0.45:0.5:250;  
NSize(NSize>numel(NeighborhoodSize))=numel(NeighborhoodSize); %limits NSize to max of defined neighborhood size

for k=kvec                                                           

    Cell_interior_adj_tmp=Cell_interior(:,:,k);
    Cell_whole_adj_tmp=Cell_whole(:,:,k);

    thres_ad=round(mean(mean(Cell_whole_adj_tmp(maskOutsideAc(:,:,k)))));

    Cell_interior_Bin_tmp=Cell_interior_adj_tmp>thres_ad;
    Cell_interior_Bin(:,:,k) = Cell_interior_Bin_tmp;
    Cell_whole_Bin_tmp=Cell_whole_adj_tmp>thres_ad;
    Cell_Bin(:,:,k)=Cell_whole_Bin_tmp;

end
disp(['Binarized images in ' num2str(toc -t) ' sec']);

clearvars mask_tmp thres_ad thres_mean thres_mask Cell_interior_adj_tmp Cell_interior_Bin_tmp

%% Calculate area
for k=kvec
    totalCellArea(k) = sum(sum(maskOutsideAc(:,:,k)))*px_area; %#ok<*AGROW>
    interiorCellArea(k) = sum(sum(maskInsideAc(:,:,k)))*px_area; 
    interiorCellAreaBin(k) = sum(sum(Cell_interior_Bin(:,:,k)))*px_area; 
end

%% Skeletonize
SED_cell=strel('disk',round(0.2*res));
SEE_cell=strel('disk',round(0.2*res));

for k=kvec            
    Cell_Bin(:,:,k)=imdilate(Cell_Bin(:,:,k),SED_cell);
    Cell_Bin(:,:,k)=imerode(Cell_Bin(:,:,k),SEE_cell);
    Cell_Bin(:,:,k)=bwmorph(Cell_Bin(:,:,k),'bridge');

    Cell_interior_Bin(:,:,k)=imdilate(Cell_interior_Bin(:,:,k) .* maskInsideAc(:,:,k),SED_cell);
    Cell_interior_Bin(:,:,k)=imerode(Cell_interior_Bin(:,:,k),SEE_cell);
    Cell_interior_Bin(:,:,k)=bwmorph(Cell_interior_Bin(:,:,k),'bridge');             
end
                            
               
interiorCellSkeleton=false(size(I));
interiorCellAreaSkeleton=zeros(size(I(1,1,:)));
for k=kvec
    interiorCellSkeleton(:,:,k)=bwmorph(Cell_interior_Bin(:,:,k) ,'thin',Inf);
    interiorCellSkeleton(:,:,k)=bwmorph(interiorCellSkeleton(:,:,k),'bridge');
    interiorCellAreaSkeleton(k)=sum(sum(interiorCellSkeleton(:,:,k)))*px_area;
end

%% Distance (2D or 3D)
interiorCellDist = zeros(size(I));    
if numel(kvec)==1 %2D  
    interiorCellDist(:,:,kvec) = bwdist(Cell_interior_Bin(:,:,kvec)+~maskInsideAc(:,:,kvec))*(res^-1); %um = pixels * (pix/um)^-1
    interiorCellDist=interiorCellDist.*single(maskInsideAc);
else  % 3D
    interiorCellDist(:,:,kvec) = bwdistsc(Cell_interior_Bin(:,:,kvec)+~maskInsideAc(:,:,kvec),[1/res,1/res,thk]);
    interiorCellDist=interiorCellDist.*single(maskInsideAc);
end
interiorCellDist(interiorCellDist==0)=NaN;
                    

%% 3D Organization
function [fracTransOfTotal, fracLongOfTotal,h_sp, image_tif]=organization3DFcn(interiorCellSkeleton,imoutput,SizeBeforeRot,orig_size,kvec,crop1,crop2,theta,h_sp,rulerLength,rulerWidth,rulerPos,maskRuler,white,uint8max,res)
    %% Organization3D
    % a simple script taking a 3D skeletonized image (I), and differentiating between
    % pixels that have a neighbour over or under (I_ou) and pixels that have a
    % neighbour to the left or right (I_lr)
    global plot_names
    for k=kvec

        S=interiorCellSkeleton(:,:,k);
        S_all=S*0;
        S_ou=S*0;
        S_lr=S*0;
        [i,j]=find(S==1);

        for ii=1:numel(i)

            S_all(i(ii),j(ii))=1;

            %neighbour left/right?
            try %#ok<TRYNC> %to avoid errors at edges
                if S(i(ii),j(ii)-1)==1 || S(i(ii),j(ii)+1)==1 %it has a neighbour directly adjacent
                    S_lr(i(ii),j(ii))=1;

                    %then, check if it has any diagonal friends
                    if S(i(ii)+1,j(ii)+1)==1
                        S_lr(i(ii)+1,j(ii)+1)=1;
                    end
                    if S(i(ii)+1,j(ii)-1)==1
                        S_lr(i(ii)+1,j(ii)-1)=1;
                    end
                    if S(i(ii)-1,j(ii)+1)==1
                        S_lr(i(ii)-1,j(ii)+1)=1;
                    end
                    if S(i(ii)-1,j(ii)-1)==1
                        S_lr(i(ii)-1,j(ii)-1)=1;
                    end
                end

            end

            %neighbour over/under?
            try %#ok<TRYNC> %to avoid errors at edges
                if S(i(ii)-1,j(ii))==1 || S(i(ii)+1,j(ii))==1 %it has a neighbour directly adjacent
                    S_ou(i(ii),j(ii))=1;

                    %then, check if it has any diagonal friends
                    if S(i(ii)+1,j(ii)+1)==1
                        S_ou(i(ii)+1,j(ii)+1)=1;
                    end
                    if S(i(ii)+1,j(ii)-1)==1
                        S_ou(i(ii)+1,j(ii)-1)=1;
                    end
                    if S(i(ii)-1,j(ii)+1)==1
                        S_ou(i(ii)-1,j(ii)+1)=1;
                    end
                    if S(i(ii)-1,j(ii)-1)==1
                        S_ou(i(ii)-1,j(ii)-1)=1;
                    end
                end
            end
            S_all(i(ii),j(ii))=1;
        end

        %% Prioritize ou over lr
        [i,j]=find(S_lr==1);
        for ii=1:numel(i)
            if S_ou(i(ii),j(ii))
                S_lr(i(ii),j(ii))=0;
            end
        end

        %%
       
        
        %%
        S_ou_lr = (S_lr|S_ou);
        fracTransOfTotal(k) = sum(S_ou(:))/sum(S_ou_lr(:));
        fracLongOfTotal(k) = sum(S_lr(:))/sum(S_ou_lr(:));
        
    end
end