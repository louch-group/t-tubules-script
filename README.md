# T-tubules script
Using bwdistsc for caclulation of 3D distances by

Mishchenko Y. (2013) A function for fastcomputation of large 
discrete Euclidean distance transforms in three or more 
dimensions in Matlab. Signal, Image and Video Processing 
DOI: 10.1007/s11760-012-0419-9.  
